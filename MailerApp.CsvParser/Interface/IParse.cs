﻿using MailerApp.CsvParser.Model;
using System.Collections.Generic;

namespace MailerApp.CsvParser.Interface
{
    public interface IParse
    {
        List<EmailData> ReadData();
    }
}
