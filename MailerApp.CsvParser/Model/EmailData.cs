﻿using CsvHelper.Configuration;

namespace MailerApp.CsvParser.Model
{
    public class EmailData
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }
    }
}
