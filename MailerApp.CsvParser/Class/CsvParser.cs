﻿using CsvHelper;
using CsvHelper.Configuration;
using MailerApp.CsvParser.Interface;
using MailerApp.CsvParser.Model;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace MailerApp.CsvParser.Class
{
    public class CsvParser : IParse
    {
        public List<EmailData> ReadData()
        {
            using (var sr = new StreamReader(System.IO.Directory.GetCurrentDirectory() +  "\\emails.csv"))
            {
                var reader = new CsvReader(sr);
                var records = reader.GetRecords<EmailData>().ToList();
                return records;
            }
        }
    }
}
