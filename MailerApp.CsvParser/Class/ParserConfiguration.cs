﻿using Autofac;

namespace MailerApp.CsvParser.Class
{
    public class ParserConfiguration : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<CsvParser>().As<Interface.IParse>();
        }
    }
}
