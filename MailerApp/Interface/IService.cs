﻿namespace MailerApp.Interface
{
    interface IService
    {
        void Start();
        void Stop();
    }
}
