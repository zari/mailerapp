﻿using Autofac;

namespace MailerApp.Interface
{
    interface IServiceConfiguration
    {
        void Configure(IContainer container);
    }
}
