﻿using Autofac;
using MailerApp.Interface;
using Topshelf;
using Topshelf.Autofac;

namespace MailerApp.Class
{
    class ServiceConfiguration: IServiceConfiguration
    {
        public void Configure(IContainer container)
        {
            HostFactory.Run(conf =>
            {
                conf.UseAutofacContainer(container);

                conf.Service<MailerService>(service =>
                {
                    // service.ConstructUsing(s => new MailerService());
                    service.ConstructUsingAutofacContainer();
                    service.WhenStarted(s => s.Start());
                    service.WhenStopped(s => s.Stop());
                });

                conf.RunAsLocalSystem();
                conf.SetServiceName("MailerServiceTopshelf");
                conf.SetDisplayName("MailerServiceTopshelf");
                conf.SetDescription("Service used for sending mails from data in .csv files");

            });
        }
    }
}
