﻿using MailerApp.Interface;
using System;

namespace MailerApp.Class
{
    class MailerService: IService
    {
        public void Start()
        {
            Console.WriteLine("MailerService started by application with Autofac...");
        }

        public void Stop()
        {
            Console.WriteLine("MailerService stopped by application with Autofac...");
        }
    }
}
