﻿using Autofac;
using MailerApp.Class;
using MailerApp.Interface;
using MailerApp.CsvParser.Class;
using System;
using MailerApp.CsvParser.Interface;
using MailerApp.CsvParser.Model;
using System.Collections.Generic;
using Hangfire;
using Microsoft.Owin.Hosting;
using System.IO;
using Serilog;

namespace MailerApp
{
    class Program
    {
        private static IContainer Container { get; set; }
        private static IDisposable webApp;

        static void Main(string[] args)
        {
            List<EmailData> mailingList;
            var builder = new ContainerBuilder();
            builder.RegisterType<MailerService>().AsSelf().As<IService>();
            builder.RegisterType<ServiceConfiguration>().As<IServiceConfiguration>();
            builder.RegisterModule<ParserConfiguration>();
            Container = builder.Build();

            Configure(Container);

            webApp = WebApp.Start<Startup>("http://MailerApp/");
            RecurringJob.AddOrUpdate(
            () => Console.WriteLine(ReadData(Container.BeginLifetimeScope().Resolve<IParse>())[0].Email),
            Cron.Minutely);

            Log.Logger = new LoggerConfiguration().WriteTo.File("logs.txt").CreateLogger();

            RecurringJob.AddOrUpdate(() => Log.Information("Log minute after minute..."),
                    Cron.Minutely());
            
            Console.ReadKey();
        }

        public static void Configure(IContainer container)
        {
            using (var scope = Container.BeginLifetimeScope())
            {
                var configuration = scope.Resolve<IServiceConfiguration>();
                configuration.Configure(container);
            }
        }

        public static List<EmailData> ReadData(IParse parse)
        {
           var emails = parse.ReadData();
           return emails;
        }
    }
}
